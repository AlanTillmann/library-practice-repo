import {client} from '../src/connection'

// client is the main object we will use to interact with our database

test("Should create a connection ", async ()=>
{
    const result = await client.query('select * from book'); // we need await because we
    // are fetching data stored on a database in the cloud
    console.log(result);
});

afterAll(()=>
{
    client.end();
})