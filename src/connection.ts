import { Client } from "pg";

export const client = new Client( // used for creating a client object
{
    user:'postgres',
    password:process.env.DBPASSWORD, // you should NEVER STORE PASSWORDS IN CODE
    database:'librarydb',
    port:5432,
    host:'35.193.90.118' //public IP address from GCP overview
})
client.connect();
