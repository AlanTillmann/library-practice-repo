// DAO (Data Access Object)
// A class that is responsible for persisting an entity
import { Book } from "../entities";

// A DAO should support the CRUD operations (Create, Read, Update, Delete)
export interface BookDAO
{
    // CREATE
    createBook(book:Book):Promise<Book>; // pass in a book and return a promise that will eventually be a book

    // READ
    getAllBooks():Promise<Book[]>;
    getBookById(bookId:number):Promise<Book>;
    
    // UPDATE
    updateBook(book:Book):Promise<Book>;

    // DELETE
    deleteBookById(bookId:number):Promise<boolean>;

}