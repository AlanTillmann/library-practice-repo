import { Book } from "../entities";

// Your service interface should have all the methods your RESTful
// will find helpful
// 1. Methods that perform CRUD operations
// 2. Business Logic operations
export default interface BookService
{
    registerBook(book:Book):Promise<Book>;

    retrieveAllBooks():Promise<Book []>;

    retrieveBookById(bookId:number):Promise<Book>;

    checkoutBookById(bookId:number):Promise<Book>;

    checkinBookById(bookId:number):Promise<Book>;

    searchByTitle(title:string):Promise<Book[]>;

    modifyBook(book:Book):Promise<Book>;

    removeBookById(bookId:number):Promise<boolean>;
}