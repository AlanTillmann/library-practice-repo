// SERVICE LAYER!
// RELIES ON THE DATA LAYER AND PROVIDES "SERVICES" TO THE API LAYER
import { BookDAO } from "../daos/book-dao";
import { BookDAOPostgres } from "../daos/book-dao-postgres";
import { Book } from "../entities";
import BookService from "./book-services";

export class BookServiceImpl implements BookService
{
    bookDAO:BookDAO = new BookDAOPostgres();

    registerBook(book: Book): Promise<Book> 
    {
        book.returnDate = 0;
        if (book.quality < 1)
        {
            book.quality = 1;
        }
        return this.bookDAO.createBook(book);
    }

    retrieveAllBooks(): Promise<Book[]> 
    {
        return this.bookDAO.getAllBooks();
    }

    retrieveBookById(bookId: number): Promise<Book> 
    {
        return this.bookDAO.getBookById(bookId);
    }

    async checkoutBookById(bookId: number): Promise<Book> 
    {
        let book:Book = await this.bookDAO.getBookById(bookId);
        book.isAvailable = false;
        book.returnDate = Date.now() + 1_209_600; // you can use underscores to make large nums more readable
        // we add the seconds for 2 weeks worth of time (1.2mil)
        book = await this.bookDAO.updateBook(book);
        return book;
    }

    // service methods also perform business logic
    async checkinBookById(bookId: number): Promise<Book> 
    {
        throw new Error("Method not implemented.");
    }

    searchByTitle(title: string): Promise<Book[]> 
    {
        throw new Error("Method not implemented.");
    }

    modifyBook(book: Book): Promise<Book> 
    {
        throw new Error("Method not implemented.");
    }

    removeBookById(bookId: number): Promise<boolean> 
    {
        throw new Error("Method not implemented.");
    }
}