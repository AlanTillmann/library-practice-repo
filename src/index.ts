// THIS IS THE API LAYER!
// RELIES ON THE SERVICES LAYER
import express from 'express';
import { Book } from './entities';
import { MissingResourceError } from './errors';
import { BookServiceImpl } from './services/book-service-impl';
import BookService from './services/book-services';

const app = express();
app.use(express.json()); // Middleware

// Our application routes should use services we create to do the heavy lifting
// Try to minimize the amount of logic in your routes that is not related directly to HTTP req and res

const bookService:BookService = new BookServiceImpl();

app.get("/books", async (req, res) =>
{
    const books:Book[] = await bookService.retrieveAllBooks();
    res.send(books);
});

app.get("/books/:id", async (req, res) =>
{
    try
    {
        const bookId = Number(req.params.id);
        const book:Book = await bookService.retrieveBookById(bookId);
        res.send(book);
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404); // can do either of these
            res.send(error);
        }
    }
});

app.post("/books", async (req, res) =>
{
    let book:Book = req.body;
    book = await bookService.registerBook(book);
    res.send(book);
});

app.patch("/books/:id/checkout", async (req, res) =>
{
    const bookid = Number(req.params.id);
    const book = await bookService.checkoutBookById(bookid);
    res.send(book);
})


app.listen(3000, ()=>
{
    console.log("Application Started");
});